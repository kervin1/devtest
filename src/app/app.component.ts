import { Component, OnInit } from '@angular/core';
import { Hit } from './shared/interfaces/api-response';

@Component( {
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
} )
export class AppComponent implements OnInit {

  title = 'devtest';
  tab = 'all';

  constructor(
  ) { }

  ngOnInit(): void {

  }

}
