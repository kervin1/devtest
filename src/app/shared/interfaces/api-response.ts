export interface ApiResponse {
  hits: Hit[];
  page: number; // pagina actual
  nbHits: number; // total de resultados
  nbPages: number; // total de paginas
  hitsPerPage: number; // resultado por pagina
  query: string;
}

export interface Hit {
  author: string;
  story_title: string;
  story_url: string;
  created_at: string;
  objectID: string;
  storage?: boolean;
}
