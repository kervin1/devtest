import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable( {
  providedIn: 'root'
} )
export class HttpService {

  constructor(
    private http: HttpClient
  ) { }

  get( endpoint: string, params?: any ): Observable<any> {
    const url = environment.api + endpoint;
    return this.http.get( url, params );
  }
}
