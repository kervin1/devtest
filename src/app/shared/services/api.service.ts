import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { ApiResponse } from '../interfaces/api-response';
import { map } from 'rxjs/operators';

export type QueryString = 'angular' | 'reactjs' | 'vuejs' | 'null';

@Injectable( {
  providedIn: 'root'
} )
export class ApiService {

  constructor(
    private http: HttpService
  ) { }

  /**
   * 
   * @param query QueryString angular | viejs | reactjs
   * @param page Page to search default 0
   * @returns List
   */
  request( query: QueryString, page = 0 ): Observable<ApiResponse> {
    return this.http.get( `search_by_date?query=${query}&page=${page}` );
  }


}
