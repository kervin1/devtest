import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../services/storage.service';
import { Hit } from '../../interfaces/api-response';

@Component( {
  selector: 'app-faves',
  templateUrl: './faves.component.html',
  styleUrls: [ './faves.component.css' ]
} )
export class FavesComponent implements OnInit {

  faves: Hit[] = [];

  constructor(
    private storage: StorageService
  ) {
    this.faves = this.storage.getItem( 'fave' );
  }

  ngOnInit() {
  }

  like( index: number ): void {
    this.faves.splice( index, 1 );
    this.storage.setItem( 'fave', this.faves );
  }

}
