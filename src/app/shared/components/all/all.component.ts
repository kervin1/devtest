import { Component, OnInit } from '@angular/core';
import { ApiService, QueryString } from '../../services/api.service';
import { Hit } from '../../interfaces/api-response';
import { NgxSpinnerService } from 'ngx-spinner';
import { StorageService } from '../../services/storage.service';
import { from } from 'rxjs';

@Component( {
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: [ './all.component.css' ]
} )
export class AllComponent implements OnInit {
  hits: Hit[] = [];
  faves: Hit[] = [];
  filters = [
    {
      value: null,
      name: 'Select your news'
    },
    {
      value: 'angular',
      name: 'Angular'
    },
    {
      value: 'reactjs',
      name: 'Reactjs'
    },
    {
      value: 'vuejs',
      name: 'Vuejs'
    }
  ];
  filter = 'null';
  pages: number;
  page = 0;
  totalItems = 0;
  hitsPerPage = 0;

  constructor(
    private apiService: ApiService,
    private spinner: NgxSpinnerService,
    private storage: StorageService

  ) {
    if ( this.storage.getItem( 'fave' ) !== null ) {
      this.faves = this.storage.getItem( 'fave' );
    }
    this.filter = this.storage.getItem( 'filter' );

    if ( this.filter !== 'null' && this.filter ) {
      this.loadData( this.filter as QueryString );
    }
  }

  ngOnInit() {
  }

  onChange( value: QueryString ): void {

    if ( value === 'null' ) {
      this.hits.length = 0;
      this.storage.setItem( 'filter', 'null' );
      return;
    }
    this.storage.setItem( 'filter', value );
    this.loadData( value );
  }

  openTab( url: string ): void {
    window.open( url );
  }

  like( hit: Hit, i: number, storage: boolean ): void {

    this.hits[ i ].storage = !storage;

    if ( this.storage.getItem( 'fave' ) === null ) {
      this.faves.push( hit );
      this.storage.setItem( 'fave', this.faves );
      return;
    }

    this.faves = this.storage.getItem( 'fave' );

    const index = this.faves.findIndex( five => five.objectID === hit.objectID );

    if ( index !== -1 ) {
      this.faves.splice( index, 1 );
      this.storage.setItem( 'fave', this.faves );
      return;
    }

    this.faves.push( hit );
    this.storage.setItem( 'fave', this.faves );

  }

  private loadData( filter: QueryString ): void {
    this.spinner.show();
    this.apiService.request( filter, this.page ).subscribe( async ( response ) => {

      this.spinner.hide();

      from( response.hits ).subscribe( hit => {
        let storage = false;

        const { author, story_title, story_url, created_at, objectID } = hit;
        const valid = author && story_title && story_url && created_at;

        if ( this.faves.length ) {
          storage = this.faves.some( f => f.objectID === hit.objectID );
        }

        if ( valid ) {
          this.hits.push( { author, story_title, story_url, created_at, objectID, storage } );
        }
      } );

      this.pages = response.nbPages;
      this.hitsPerPage = response.hitsPerPage;
      this.totalItems = response.hitsPerPage * response.nbPages;
    }, () => this.spinner.hide() );
  }


  pageChanged( page: number ): void {
    this.page = page;
    this.loadData( this.filter as QueryString );
  }

}
